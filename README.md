#Calories App


##Breif

Calories app built using Laravel + React.JS frameworks.

##Demo

You can find a video demo of the application in the root of this repo, download file named **DemoVideo.mov**

##Requirements (Tested on)

- composer *v1*
- node *v16.5.0*
- npm *v8.3.0*
- Nginx *latest*
- MySQL *v8*



##Installation

- run `composer install`
- copy `.env.example` to `.env` ,and change database values to match the new database you have setup on your environment.
- change all base urls in .env file to match your local env.
- run `php artisan key:generate --ansi`
- run `php artisan migrate`
- run `php artisan passport:install`
- run `php artisan setup:env-passport-tokens`
- run `php artisan db:seed`
- run `npm install` then `npm run dev`



##Testing 

The Database seeder will populate an admin user (admin@admin.com) along with ten randomly generated fake test users. all test users have the password set to `password`.