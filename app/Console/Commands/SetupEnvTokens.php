<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Laravel\Passport\Passport;

class SetupEnvTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:env-passport-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copies Passport tokens to env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get newest password grant client
        $client = \Laravel\Passport\Client::where('password_client', 1)
            ->where('revoked',0)
            ->orderBy('created_at', 'DESC')
            ->first();
        if(!$client){
            $this->output->error("No passport clients found, please run `php artisan passport:install`");
            return 0;
        }

        //update env file values
        $this->updateEnvValues([
            'MIX_REACT_APP_CLIENT_ID' => $client->id,
            'MIX_REACT_APP_CLIENT_SECRET' => $client->secret,
        ]);
        $this->output->success(".Env file updated with latest passport client token: {$client->id}");
        return 1;
    }

    private function updateEnvValues($newValues) {
        $path = base_path('.env');
        $envFileText = file_get_contents($path);

        $envFileArray = explode(PHP_EOL, $envFileText);
        $newEnv = [];

        $updatedKeys = [];

        //update existing vars
        foreach($envFileArray as $k=>$line) {
            $lineArray = explode('=', $line);
            $key = $lineArray[0];
            if(!empty($key) && isset($newValues[$key])){
                $updatedKeys[] = $key;
                $line = "$key={$newValues[$key]}";
            }
            $newEnv[] = $line;
        }

        //check none existing vars and append them
        foreach ($newValues as $key=>$newValue){
            if(!in_array($key, $updatedKeys)){
                $newEnv[] = "$key={$newValue}";
            }
        }

        //update .env file with new lines
        return file_put_contents($path,implode(PHP_EOL,$newEnv));
    }
}
