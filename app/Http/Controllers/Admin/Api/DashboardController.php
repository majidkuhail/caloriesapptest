<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\FoodEntry;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stats(Request $request)
    {
        $last7DaysDate = Carbon::now()->subDays(7)->startOfDay();
        $weekBeforeDate = $last7DaysDate->clone()->subDays(7);
        return response()->json([
            'data' => [
                'food_entries'=> [
                    'last_7days' => FoodEntry::where('created_at', '>=', $last7DaysDate)->count(),
                    'week_before' => FoodEntry::where('created_at', '>=', $weekBeforeDate)
                        ->where('created_at', 'lt', $last7DaysDate)
                        ->count(),
                    'total' => FoodEntry::count()
                ],
                'users'=> [
                    'last_7days' => User::where('created_at', '>=', $last7DaysDate)->count(),
                    'week_before' => User::where('created_at', '>=', $weekBeforeDate)
                        ->where('created_at', 'lt', $last7DaysDate)
                        ->count(),
                    'total' => User::count()
                ]
            ]
        ]);
    }
}
