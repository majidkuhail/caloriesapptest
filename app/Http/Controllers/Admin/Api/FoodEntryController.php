<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFoodEntryRequest;
use App\Http\Resources\FoodEntryResource;
use App\Models\FoodEntry;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FoodEntryController extends Controller
{
    /**
     * Return all user entries
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(User $user, Request $request)
    {
        $items = $user->food_entries();

        $limit = $request->has('limit') ? (int) $request->limit : 10;

        if($request->has('order')){
            $items->orderBy($request->order, $request->has('order_dir')? $request->order_dir :'DESC');
        }else{
            $items->orderBy('eaten_at','DESC');
        }

        $countableCount = $user->food_entries()
            ->where('diet_cheating', 0)->count();
        $totalCalories = $user->food_entries()
            ->where('diet_cheating', 0)->sum('calories');

        $stats = [
            'count' => $user->food_entries()->count(),
            'total_calories' => $totalCalories,
            'daily_average' => $totalCalories / $countableCount,
        ];
        return FoodEntryResource::collection($items->paginate($limit))
            ->additional(compact('stats'));
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stats(User $user, Request $request)
    {
        $numOfDays = $user->food_entries()
            ->where('diet_cheating', 0)->groupBy(DB::raw('DAY(eaten_at)'))->count();
        $totalCalories = $user->food_entries()
            ->where('diet_cheating', 0)->sum('calories');

        $last7DaysDate = Carbon::now()->subDays(7)->startOfDay();
        $last7Items = $user->food_entries()->where('created_at', '>=', $last7DaysDate);
        $last7Calories = (clone $last7Items)->sum('calories');
        $last7NumOfDays = (clone $last7Items)->groupBy(DB::raw('DAY(eaten_at)'))->count();

        $stats = [
            'count' => $user->food_entries()->count(),
            'total_calories' => $totalCalories,
            'daily_average' => $numOfDays > 0 ? $totalCalories / $numOfDays : 0,
            'daily_average_last7' => $last7NumOfDays > 0 ? $last7Calories / $last7NumOfDays : 0,
        ];
        return response()->json(['data'=>compact('stats')]);
    }

    /**
     * Get food entry
     * @param FoodEntry $foodEntry
     * @param Request $request
     * @return FoodEntryResource
     */
    public function show(FoodEntry $foodEntry, Request $request)
    {
        return new FoodEntryResource($foodEntry);
    }

    /**
     * Store new food entry
     * @param StoreFoodEntryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(User $user, StoreFoodEntryRequest $request)
    {
        //store
        $user->food_entries()->create([
            'name' => $request->get('name'),
            'calories' => $request->get('calories'),
            'eaten_at' => $request->get('eaten_at'),
            'diet_cheating' => (int) $request->get('diet_cheating')
        ]);

        return $this->jsonSuccess("Entry added");
    }

    /**
     * Update food entry
     * @param FoodEntry $foodEntry
     * @param StoreFoodEntryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(FoodEntry $foodEntry, StoreFoodEntryRequest $request)
    {
        //update model
        $foodEntry->update([
            'name' => $request->get('name'),
            'calories' => $request->get('calories'),
            'eaten_at' => $request->get('eaten_at'),
            'diet_cheating' => (int) $request->get('diet_cheating')
        ]);

        return $this->jsonSuccess("Changes Saved");
    }

    /**
     * @param FoodEntry $foodEntry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(FoodEntry $foodEntry, Request $request)
    {
        $foodEntry->deleteOrFail();

        return $this->jsonSuccess("Entry Deleted");
    }
}

