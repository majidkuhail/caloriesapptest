<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $items = User::NotAdmin();

        $limit = $request->has('limit') ? (int) $request->limit : 10;

        if($request->has('order')){
            $items->orderBy($request->order, $request->has('order_dir')? $request->order_dir :'DESC');
        }else{
            $items->orderBy('created_at','DESC');
        }

        return UserResource::collection($items->paginate($limit));
    }

    /**
     * @param User $user
     * @param Request $request
     * @return UserResource
     */
    public function show(User $user, Request $request)
    {
        return new UserResource($user);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(User $user, Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:150',
            'last_name' => 'required|max:150',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'calories_limit' => 'required|integer|min:0',
        ]);

        //update model
        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'calories_limit' => $request->get('calories_limit'),
        ]);

        return $this->jsonSuccess("Changes Saved");
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(User $user, Request $request)
    {
        $user->deleteOrFail();

        return $this->jsonSuccess("User Deleted");
    }
}
