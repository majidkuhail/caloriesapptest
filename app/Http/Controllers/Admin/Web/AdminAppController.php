<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminAppController extends Controller
{

    public function index(Request $request)
    {
        return view('admin/pages/app');
    }
}
