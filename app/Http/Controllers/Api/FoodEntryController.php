<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFoodEntryRequest;
use App\Http\Resources\FoodEntryResource;
use App\Models\FoodEntry;
use App\Models\User;
use Illuminate\Http\Request;

class FoodEntryController extends Controller
{
    /**
     * Return all users entries
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $items = $user->food_entries()
            ->orderBy('eaten_at', 'DESC');

        return FoodEntryResource::collection($items->get());
    }

    /**
     * Get food entry
     * @param FoodEntry $foodEntry
     * @param Request $request
     * @return FoodEntryResource
     */
    public function show(FoodEntry $foodEntry, Request $request)
    {
        //check user permission to access entry
        $this->checkPermission($foodEntry, $request);

        return new FoodEntryResource($foodEntry);
    }

    /**
     * Store new food entry
     * @param StoreFoodEntryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreFoodEntryRequest $request)
    {
        /** @var User $user */
        $user = $request->user();

        //store
        $user->food_entries()->create([
            'name' => $request->get('name'),
            'calories' => $request->get('calories'),
            'eaten_at' => $request->get('eaten_at'),
            'diet_cheating' => (int) $request->get('diet_cheating')
        ]);

        return $this->jsonSuccess("Entry added");
    }

    /**
     * Update food entry
     * @param FoodEntry $foodEntry
     * @param StoreFoodEntryRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(FoodEntry $foodEntry, StoreFoodEntryRequest $request)
    {
        //check user permission to access entry
        $this->checkPermission($foodEntry, $request);

        //update model
        $foodEntry->update([
            'name' => $request->get('name'),
            'calories' => $request->get('calories'),
            'eaten_at' => $request->get('eaten_at'),
            'diet_cheating' => (int) $request->get('diet_cheating')
        ]);

        return $this->jsonSuccess("Changes Saved");
    }

    public function destroy(FoodEntry $foodEntry, Request $request)
    {
        //check user permission to access entry
        $this->checkPermission($foodEntry, $request);

        $foodEntry->deleteOrFail();

        return $this->jsonSuccess("Entry Deleted");
    }

    private function checkPermission(FoodEntry $foodEntry, Request $request)
    {
        if($request->user()->id !== $foodEntry->user_id){
            abort(403, 'No permission.');
        }
        return true;
    }
}
