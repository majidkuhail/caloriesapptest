<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'calories_limit' => User::DEFAULT_CALORIES_LIMIT
        ]);

        $user->assignRole(User::ROLE_USER);

        return $this->jsonSuccess('Registered successfully', ['user_id'=>$user->id]);
    }


    /**
     * @param Request $request
     * @return UserResource
     */
    public function me(Request $request)
    {
        return new UserResource($request->user());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateCaloriesLimit(Request $request)
    {
        $this->validate($request, [
            'calories_limit' => 'required|integer|min:500,max:10000',
        ]);

        $request->user()->update([
            'calories_limit' => $request->get('calories_limit')
        ]);

        return $this->jsonSuccess("Changes Saved");
    }
}
