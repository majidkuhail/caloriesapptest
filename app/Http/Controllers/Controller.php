<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param string $msg
     * @param null $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonSuccess($msg = '', $data = null, $status = 200){
        return response()->json( array(
                'status' => 'success',
                'message' => $msg,
                'data' => $data
            )
            , $status);
    }

    /**
     * @param string $msg
     * @param null $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($msg = '', $data = null, $status = 400){
        return response()->json( array(
                'status' => 'error',
                'message' => $msg,
                'data' => $data
            )
            , $status);
    }
}
