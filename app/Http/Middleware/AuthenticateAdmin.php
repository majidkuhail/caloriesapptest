<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(
            Auth::guard('api')->check() &&
            Auth::guard('api')->user()->isAdmin()
        ) {
            return $next($request);
        }
        if ($request->expectsJson()) {
            return response('Unauthorized.', 401);
        } else {
            abort(401);
        }

    }
}
