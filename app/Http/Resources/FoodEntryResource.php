<?php

namespace App\Http\Resources;

use App\Models\FoodEntry;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class FoodEntryResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'calories' => $this->calories,
            'eaten_at' => $this->eaten_at,
            'diet_cheating' => (bool) $this->diet_cheating,

            $this->mergeWhen(Auth::user()->isAdmin(), [
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
            ]),
        ];
    }
}
