<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->name,
            'email' => $this->email,
            'calories_limit' => $this->calories_limit,
            'role' => $this->isAdmin() ? 'admin' : 'user',

            $this->mergeWhen(Auth::user()->isAdmin(), [
               'created_at' => $this->created_at->format('d/M/Y')
            ]),
        ];
    }
}
