<?php
namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class FoodEntry extends Model
{
    use HasUuid;

    protected $dates = ['eaten_at'];
    protected $guarded = [];

    //RELATIONS
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
