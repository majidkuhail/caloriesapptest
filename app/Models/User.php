<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, HasUuid;

    //Roles
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    //Default Calorie limit
    const DEFAULT_CALORIES_LIMIT = 2100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'calories_limit'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = ['name'];

    //RELATIONS
    public function food_entries(){
        return $this->hasMany(FoodEntry::class,'user_id', 'id');
    }

    //SCOPES
    public function scopeAdmins($query){
        return $query->role(self::ROLE_ADMIN);
    }

    public function scopeNotAdmin($query){
        return $query->role(self::ROLE_USER);
    }

    //APPENDS
    public function getNameAttribute(){
        $name = [];
        if(!empty($this->first_name)){
            $name[] = $this->first_name;
        }
        if(!empty($this->last_name)){
            $name[] = $this->last_name;
        }
        return implode(' ', $name);
    }


    //METHODS


    /**
     * @return bool
     */
    public function isAdmin(){
        return $this->hasAnyRole([self::ROLE_ADMIN]);
    }

    /**
     * @return bool
     */
    public function isUser(){
        return $this->hasAnyRole([self::ROLE_USER]);
    }

}
