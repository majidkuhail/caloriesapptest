<?php
namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait HasUuid
{
    /**
     * This is used by Eloquent models to determine if they increment or not
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }
    public static function bootHasUuid()
    {
        static::creating(function ($model) {
            $model->incrementing = false;
            $model->attributes[$model->getKeyName()] = (string)$model->generateNewUuid();
        });
    }
    public function generateNewUuid()
    {
        return Uuid::uuid4();
    }

    public function getKeyType()
    {
        return 'string';
    }
}
