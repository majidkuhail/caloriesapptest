<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDietCheatingToFoodEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_entries', function (Blueprint $table) {
            $table->boolean('diet_cheating')->default(0)->after('calories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_entries', function (Blueprint $table) {
            $table->dropColumn(['diet_cheating']);
        });
    }
}
