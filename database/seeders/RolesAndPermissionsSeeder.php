<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        if(Role::where('name',User::ROLE_USER)->count()==0){
            $role = Role::create(['name'=>User::ROLE_USER]);
        }
        if(Role::where('name',User::ROLE_ADMIN)->count()==0){
            $role = Role::create(['name'=>User::ROLE_ADMIN]);
            $role->givePermissionTo(Permission::all());
        }
    }
}
