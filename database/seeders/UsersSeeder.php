<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::count() > 0){
            return;
        }
        //
        $admins = [
            [
                'first_name' => 'Admin',
                'last_name' => 'User',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password')
            ]
        ];
        foreach ($admins as $adminData){
            $admin = User::create($adminData);
            $admin->assignRole(User::ROLE_ADMIN);
        }

        $users = \App\Models\User::factory(10)->create();
        foreach ($users as $user) {
            $user->assignRole(User::ROLE_USER);
        }
    }
}
