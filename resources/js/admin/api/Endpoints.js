const Endpoints = {
    DASHBOARD_STATS: "/dashboard/stats",

    USERS: "/users",

    FOOD_ENTRIES: "/food_entries",
    USER_FOOD_ENTRIES: "/user_food_entries",
};
export default Endpoints;
