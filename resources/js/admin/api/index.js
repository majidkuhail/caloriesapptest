import {adminApiUrl} from "../utils";
import Endpoints from "../api/Endpoints";
import {apiUrl} from "../../utils";

export function api_getDashboardStats(success, error) {
    axios.get(adminApiUrl(Endpoints.DASHBOARD_STATS))
        .then(success)
        .catch(error);
}

export function api_getUsers(page, filters, success, error) {
    axios({
            url: adminApiUrl(Endpoints.USERS),
            method: "GET",
            params: {page: page, ...filters}
        })
        .then(success)
        .catch(error);
}
export function api_deleteUser(userId, success, error) {
    axios({
        url: adminApiUrl(Endpoints.USERS) + "/" + userId,
        method: "DELETE"
    })
        .then(success)
        .catch(error);
}

export function api_getUser(userId, success, error) {
    axios({
        url: adminApiUrl(Endpoints.USERS) + "/" + userId,
        method: "GET"
    })
        .then(success)
        .catch(error);
}

export function api_updateUser(userId, data, success, error) {
    axios({
        url: adminApiUrl(Endpoints.USERS) + "/" + userId,
        method: "PUT",
        data: data,
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}


//FOOD ENTRIES

export function api_getFoodEntriesStats(userId, success, error) {
    axios.get(adminApiUrl(Endpoints.USER_FOOD_ENTRIES+'/'+userId+'/stats'))
        .then(success)
        .catch(error);
}


export function api_deleteFoodEntry(id, success, error) {
    axios({
        url: adminApiUrl(Endpoints.FOOD_ENTRIES) + "/" + id,
        method: "DELETE"
    })
        .then(success)
        .catch(error);
}
export function api_getFoodEntry(entryId, success, error) {
    axios.get(adminApiUrl(Endpoints.FOOD_ENTRIES + '/' + entryId))
        .then(success)
        .catch(error);
}

export function api_storeFoodEntry(userId, data, success, error) {
    axios({
        url: adminApiUrl(Endpoints.USER_FOOD_ENTRIES + '/' + userId),
        method: 'POST',
        data: data,
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}

export function api_updateFoodEntry(entryId, data, success, error) {
    axios({
        url: adminApiUrl(Endpoints.FOOD_ENTRIES + '/' + entryId),
        method: 'PUT',
        data: data,
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}
