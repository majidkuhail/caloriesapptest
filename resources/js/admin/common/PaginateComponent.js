import BaseComponent from "../../common/BaseComponent";
import _get from "lodash/get";
import _last from "lodash/last";

export default class PaginateComponent extends BaseComponent
{
    constructor(props){
        super(props)
        this.state = {
            url: '',

            items: [],
            response_headers: {},
            response_active_filters: [],
            pagination: {
                current_page: 1,
                total_pages: 1,
                per_page: 10,
                total: 0,
                from: 0,
                to: 0,
            },

            // parameters
            default_parameters: ['order','order_dir'],
            order: '',
            order_dir: 'asc',
            working: true,
            filters: {},
            show_filters: false,
            parameters: []

        }
    }

    getParameters = (page) =>{
        const params = new Object();
        this.state.default_parameters.map((value) => {
            if(this.state[value] !== '' && this.state[value] != null){
                params[value] = this.state[value];
            }
        })
        this.state.parameters.map((value) => {
            const val = _get(this.state,value,null);
            if(val !== '' && val != null){
                if(value.indexOf('.')> 0) {
                    params[_last( value.split('.'))] = val;
                }else{
                    params[value] = val;
                }
            }
        });

        if(page > 0) {
            params.page = page;
        }else if(page === undefined){
            params.page = this.state.pagination.current_page;
        }

        if(this.state.pagination.per_page){
            params.limit = this.state.pagination.per_page;
        }

        return params;
    }

    fetchData = (page)=>{
        this.setState({working: true});
        const urlParams = this.getParameters(page);
        axios.get(this.state.url , { params: urlParams})
            .then(response => {
                const responseData = response.data;
                let response_active_filters = [];
                if(response.headers.map
                    && response.headers.map['active-filters'] !== undefined
                    && response.headers.map['active-filters'] !== ''
                ){
                    response_active_filters = JSON.parse(response.headers.map['active-filters']);
                }
                this.setState({
                    items: responseData.data,
                    pagination: {
                        current_page: responseData.meta?.current_page,
                        total_pages: responseData.meta?.last_page,
                        per_page: responseData.meta?.per_page,
                        total: responseData.meta?.total,
                    },
                    response_headers: response.headers.map,
                    response_active_filters,
                    working: false,
                })
            })
            .catch( (error) =>{
                console.log(error);
                this.setState({items: [], working: false});
                if(error.status == 401) {
                    // swal('Oops','Your session has expired, please refresh the page','error')
                }
            })
    }

    handleChangePage = page => {
        console.log(page)
        this.fetchData(page)
    }

    handleChangeRowsPerPage = per_page => {
        const {pagination} = this.state;
        console.log(per_page);
        pagination.per_page = per_page;
        this.setState({pagination},()=>{
            this.fetchData(1);
        });
    }

    handleSortChange = newOrder =>{
        this.setState({
            order: newOrder,
            order_dir: (newOrder === this.state.order && this.state.order_dir === 'desc')?'asc':'desc'
        }, ()=>{
            this.fetchData(1);
        });
    }
}
