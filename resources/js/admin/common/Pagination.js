import React from 'react';
import PropTypes from 'prop-types';
import {Icon, IconButton, TablePagination} from "@mui/material";

class Pagination extends React.Component{
    constructor(props){
        super(props)
    }

    defaultPropTypes = {
        pagination: PropTypes.isRequired,
        onPageChange: PropTypes.func.isRequired,
        onRowsPerPageChange: PropTypes.func.isRequired,
    }

    handleChangePage = event => page=>{
        this.props.onPageChange(page);
    }

    handleChangeRowsPerPage = event =>{
        this.props.onRowsPerPageChange(event.target.value);
    }

    render(){
        const {
            pagination,
            onPageChange
        } = this.props;
        return (
            <TablePagination
                rowsPerPageOptions={[1, 5, 10, 25]}
                // colSpan={3}
                count={pagination.total}
                rowsPerPage={parseInt(pagination.per_page)}
                page={pagination.current_page - 1}
                SelectProps={{
                    inputProps: { 'aria-label': 'Show per page' },
                    native: true,
                }}
                labelRowsPerPage=""
                onPageChange={this.handleChangePage}
                onRowsPerPageChange={this.handleChangeRowsPerPage}
                ActionsComponent={()=> (
                    <React.Fragment>
                        <IconButton
                            onClick={()=>onPageChange(1)}
                            disabled={pagination.current_page === 1}
                            aria-label="First Page"
                        >
                            <Icon>first_page</Icon>
                        </IconButton>
                        <IconButton
                            onClick={()=>onPageChange(pagination.current_page-1)}
                            disabled={pagination.current_page === 1}
                            aria-label="Previous Page"
                        >
                            <Icon>keyboard_arrow_left</Icon>
                        </IconButton>
                        <IconButton
                            onClick={()=>onPageChange(pagination.current_page+1)}
                            disabled={pagination.total_pages === pagination.current_page}
                            aria-label="Next Page"
                        >
                            <Icon>keyboard_arrow_right</Icon>
                        </IconButton>
                        <IconButton
                            onClick={()=>onPageChange(pagination.total_pages)}
                            disabled={pagination.total_pages === pagination.current_page}
                            aria-label="Last Page"
                        >
                            <Icon>last_page</Icon>
                        </IconButton>
                    </React.Fragment>
                )}
            />
        )
    }
}
export default Pagination;
