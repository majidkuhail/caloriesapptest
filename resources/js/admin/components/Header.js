import React from "react";
import {AppBar, Button, Icon, Toolbar, Typography} from "@mui/material";
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import {getUrl} from "../../utils";
import {logout as AuthLogout} from "../../api/Auth";
import routes from "../../routes/routes";

@inject('authStore')
@observer
class Header extends React.Component
{
    handleLogout = () => {
        AuthLogout();
        setTimeout(() => {
            location.href = getUrl(routes.login.path);
        }, 300);
    }

    render() {
        const {
            authStore
        } = this.props;
        const auth = authStore && authStore.checkAdmin
        const user = auth ? authStore.user : null;
        return (
            <AppBar position="fixed" color="secondary" sx={{bgcolor:'text.primary'}}>
                <Toolbar>
                    <Typography variant="h3" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                        Calories App - Admin Area
                    </Typography>
                    {auth && (
                        <React.Fragment>
                            <Button variant="text" color="inherit" startIcon={<Icon>manage_accounts</Icon>}>
                                {user.name}
                            </Button>
                            <Button color="inherit" variant="outlined" size="small" onClick={this.handleLogout}>Sign out</Button>
                        </React.Fragment>
                    )}
                </Toolbar>
            </AppBar>
        );
    }
}

export default withRouter(Header);
