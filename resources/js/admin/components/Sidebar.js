import React from "react";
import {Link, withRouter} from "react-router-dom";
import _map from "lodash/map";
import {
    Box,
    Divider,
    Drawer, Icon, IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ListSubheader,
    Toolbar
} from "@mui/material";
import routes from "../routes/routes";

const SidebarLinks = [
    {
        label: 'General',
        items: [
            {label:'Dashboard', route: routes.dashboard.path, icon:'<span class="material-icons MuiIcon-root">dashboard</span>'},
        ]
    },
    {
        label: 'Manage Users',
        items: [
            {label:'Users List', route: routes.users.path, icon:'<span class="material-icons MuiIcon-root">people</span>'},
        ]
    },
];

class Sidebar extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            openNavs: []
        }
    }

    handleToggleNav = navId =>{
        const {openNavs} = this.state;
        const index = this.state.openNavs.indexOf(navId)
        if(index >= 0){
            openNavs.splice(index);
        }else{
            openNavs.push(navId)
        }
        this.setState({openNavs})
    }

    render() {
        const {
            openNavs,
        } = this.state;
        const currentRoute = '';
        return (
            <Drawer
                variant={'permanent'}
                anchor="left"
                open={true}
                sx={{
                    position: 'static',
                    width: 240,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: 240,
                        boxSizing: 'border-box',
                        top: 64,
                    },
                }}
            >
                <Divider />
                {_map(SidebarLinks,(category, key)=>{
                    return (
                        <List
                            key={key}
                            subheader={<ListSubheader>{category.label}</ListSubheader>}
                            disablePadding
                        >
                            {_map(category.items,(item, k)=>{
                                if(item.children){
                                    const isItemSelected = item.children.filter(child=>{
                                        return currentRoute === child.route;
                                    }).length>0;
                                    const sideNavId = `side_nav_${key}_${k}`;
                                    const navOpen = openNavs.indexOf(sideNavId)!==-1 || isItemSelected;
                                    return (
                                        <List key={k} disablePadding>
                                            <ListItem button divider selected={currentRoute === item.route}>
                                                <ListItemIcon><span dangerouslySetInnerHTML={{__html: item.icon}} /></ListItemIcon>
                                                <ListItemText primary={item.label} />
                                                <IconButton size="small" onClick={(e)=>{e.preventDefault();e.stopPropagation();this.handleToggleNav(sideNavId)}}>
                                                    {navOpen ? <Icon>expand_less</Icon> : <Icon>expand_more</Icon>}
                                                </IconButton>
                                            </ListItem>
                                            {_map(item.children,(child, i)=> {
                                                const isChildSelected = currentRoute === child.route;
                                                return (
                                                    <ListItem
                                                        button
                                                        divider
                                                        key={`c${i}`}
                                                        dense
                                                        selected={isChildSelected}
                                                        style={{backgroundColor:'rgba(0,0,0,0.2)'}}
                                                        onClick={()=>{this.handleListClick(child)}}
                                                    >
                                                        <ListItemText primary={child.label} />
                                                    </ListItem>
                                                )
                                            })}
                                        </List>
                                    )
                                }
                                const isItemSelected = (currentRoute === item.route || currentRoute.indexOf(`${item.route}/`)===0);
                                return (
                                    <ListItem
                                        button
                                        divider
                                        key={k}
                                        selected={isItemSelected}
                                        component={Link} to={item.route}
                                    >
                                        <ListItemIcon><span dangerouslySetInnerHTML={{__html: item.icon}} /></ListItemIcon>
                                        <ListItemText primary={item.label} />
                                    </ListItem>
                                )

                            })}
                        </List>
                    )
                })}
            </Drawer>
        );
    }
}

export default withRouter(Sidebar);
