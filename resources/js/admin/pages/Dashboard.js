import React from "react";
import {inject, observer} from "mobx-react";
import BaseComponent from "../../common/BaseComponent";
import {withRouter} from "react-router-dom";
import {Card, CardContent, Grid, Skeleton, Typography, Divider} from "@mui/material";
import {api_getDashboardStats} from "../api";
import {toast} from "react-toastify";

class Dashboard extends BaseComponent
{
    constructor(props) {
        super(props);
        this.state = {
            stats: {},

            fetching: true
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData = ()=>{
        api_getDashboardStats(
            (response) => {
                const data = response?.data?.data || {}
                this.setState({
                    stats: data,
                    fetching: false
                });
            },
            (error) => {
                toast.error('Problem fetching stats.')
            }
        )
    }

    render() {
        const {
            stats,
            fetching
        } = this.state;
        return (
            <div>
                <Typography variant="h2" component="h1" sx={{mb: 3}}>Dashboard</Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardContent>
                                <Typography component="h2" variant="h5" color="primary" gutterBottom>
                                    Food Entries
                                </Typography>
                                {fetching ? (
                                    <React.Fragment>
                                        <Skeleton width="80%" height={20}/>
                                        <Skeleton width="80%" height={20}/>
                                        <Skeleton width="80%" height={20}/>
                                    </React.Fragment>
                                ):(
                                    <React.Fragment>
                                        <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.food_entries?.last_7days || 0} <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>New entries (last 7 days)</Typography></Typography>
                                        <Divider sx={{mt:1, mb:1}}/>
                                        <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.food_entries?.week_before || 0} <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Entries the week before</Typography></Typography>
                                        <Divider sx={{mt:1, mb:1}}/>
                                        <Typography component="p" variant="h3" sx={{display: 'flex', alignItems: 'center'}}>{stats.food_entries?.total || 0} <Typography variant="h5" component="span" color="text.secondary" sx={{ml:1}}>Total entries</Typography></Typography>
                                    </React.Fragment>
                                )}
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardContent>
                                <Typography component="h2" variant="h5" color="primary" gutterBottom>
                                    Users
                                </Typography>
                                {fetching ? (
                                    <React.Fragment>
                                        <Skeleton width="80%" height={20}/>
                                        <Skeleton width="80%" height={20}/>
                                        <Skeleton width="80%" height={20}/>
                                    </React.Fragment>
                                ):(
                                    <React.Fragment>
                                        <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.users?.last_7days || 0} <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>New users (last 7 days)</Typography></Typography>
                                        <Divider sx={{mt:1, mb:1}}/>
                                        <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.users?.week_before || 0} <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Users the week before</Typography></Typography>
                                        <Divider sx={{mt:1, mb:1}}/>
                                        <Typography component="p" variant="h3" sx={{display: 'flex', alignItems: 'center'}}>{stats.users?.total || 0} <Typography variant="h5" component="span" color="text.secondary" sx={{ml:1}}>Total users</Typography></Typography>
                                    </React.Fragment>
                                )}
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withRouter(Dashboard);
