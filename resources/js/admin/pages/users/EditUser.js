import {withRouter} from "react-router-dom";
import BaseComponent from "../../../common/BaseComponent";
import {
    Card,
    CardContent,
    CardHeader,
    CardActions,
    Container,
    Grid,
    LinearProgress,
    Stack,
    TextField,
    Typography, InputAdornment
} from "@mui/material";
import React from "react";
import LoadingButton from "@mui/lab/LoadingButton";
import FormErrors from "../../../form/FormErrors";
import {api_getUser, api_updateUser} from "../../api";
import {toast} from "react-toastify";
import UserFoodEntriesList from "./UserFoodEntriesList";

class EditUser extends BaseComponent
{
    constructor(props) {
        super(props);
        this.state = {
            user:{},
            fetching: true,

            first_name: '',
            last_name: '',
            email: '',
            calories_limit: '',

            working: false,
            formErrors: new FormErrors()
        }
    }

    componentDidMount() {
        if (
            this.props.match &&
            this.props.match.params &&
            this.props.match.params.id
        ) {
            this.fetchData();
        }
    }

    fetchData = ()=>{
        this.setState({fetching: true})
        api_getUser(this.props.match.params.id,
            (response)=>{
                const data = response.data?.data || {};
                this.setState({
                    user: data,

                    first_name: data.first_name,
                    last_name: data.last_name,
                    email: data.email,
                    calories_limit: data.calories_limit,
                    fetching: false,
                });
            },
            (error)=>{
                toast.error('Problem fetching user details');
            })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.working) {
            return;
        }
        this.state.formErrors.forget();
        this.setState({ working: true });

        const data = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            calories_limit: this.state.calories_limit,
        }

        api_updateUser(this.props.match.params.id ,data,
            (response)=>{
                toast.success('Changes saved');
                this.setState({working: false})
            },
            (error)=>{
                this.state.formErrors.processResponse(
                    error.response,
                    'Please check your inputs'
                );
                this.setState({ working: false });
            });

    }

    render() {
        const {
            fetching,
            user,

            first_name,
            last_name,
            email,
            calories_limit,

            working,
            formErrors
        } = this.state;

        if(fetching){
            return (
                <LinearProgress color="secondary"/>
            )
        }

        return (
            <Container maxWidth="lg">
                <Typography variant="h2" component="h1" sx={{mb: 3}}>Edit User: "{user.name}"</Typography>

                <form onSubmit={this.handleSubmit}>
                <Card>
                    <CardHeader title="User Details" titleTypographyProps={{color:"primary"}}/>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6} md={4} lg={3}>
                                <TextField
                                    label="First Name"
                                    value={first_name}
                                    margin="none"
                                    size="small"
                                    placeholder="Joe"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('first_name')}
                                    error={formErrors.has('first_name')}
                                    helperText={formErrors.get('first_name')}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={4} lg={3}>
                                <TextField
                                    label="Last Name"
                                    value={last_name}
                                    margin="none"
                                    size="small"
                                    placeholder="Doe"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('last_name')}
                                    error={formErrors.has('last_name')}
                                    helperText={formErrors.get('last_name')}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={4} lg={3}>
                                <TextField
                                    label="Email Address"
                                    value={email}
                                    margin="none"
                                    size="small"
                                    placeholder="name@domain.com"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('email')}
                                    error={formErrors.has('email')}
                                    helperText={formErrors.get('email')}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={4} lg={3}>
                                <TextField
                                    label="Calories"
                                    value={calories_limit}
                                    margin="none"
                                    type="number"
                                    size="small"
                                    min="0"
                                    fullWidth
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">KCal</InputAdornment>
                                    }}
                                    disabled={working}
                                    onChange={this.handleInputChange('calories_limit')}
                                    error={formErrors.has('calories_limit')}
                                    helperText={formErrors.get('calories_limit')}
                                    // required
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Stack direction="row" width="100%" justifyContent="flex-end">
                            <LoadingButton
                                type="submit"
                                variant="contained"
                                color="primary"
                                size="large"
                                loading={working}
                            >Save Changes</LoadingButton>
                        </Stack>
                    </CardActions>
                </Card>
                </form>

                <UserFoodEntriesList user={user}/>
            </Container>
        );
    }
}

export default withRouter(EditUser);
