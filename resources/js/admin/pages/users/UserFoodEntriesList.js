import PropTypes from "prop-types";
import React from "react";
import {withRouter} from "react-router-dom";
import {
    Card,
    CardContent,
    Grid,
    Skeleton,
    Typography,
    Divider,
    Container,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableSortLabel,
    TableBody,
    TableContainer,
    Paper,
    Fade,
    LinearProgress,
    Avatar,
    IconButton,
    Icon, Dialog, DialogTitle, DialogActions, Button, TableFooter, CardHeader, CircularProgress, Box, Chip
} from "@mui/material";
import {api_deleteFoodEntry, api_getFoodEntriesStats} from "../../api";
import {toast} from "react-toastify";
import PaginateComponent from "../../common/PaginateComponent";
import _merge from "lodash/merge";
import {adminApiUrl} from "../../utils";
import Endpoints from "../../api/Endpoints";
import LoadingButton from "@mui/lab/LoadingButton";
import Pagination from "../../common/Pagination";
import dayjs from "dayjs";
import UserFoodEntryForm from "./UserFoodEntryForm";

class UserFoodEntriesList extends PaginateComponent
{
    constructor(props) {
        super(props);
        this.state = _merge(this.state,{
            url: adminApiUrl(Endpoints.USER_FOOD_ENTRIES + '/' + this.props.user.id),
            order: 'eaten_at',
            order_dir: 'desc',
            parameters: [],

            filters:{

            },

            openDialog: null,
            deleting: false,

            stats: {},
            fetching_stats: true,
        });
    }

    componentDidMount() {
        this.fetchData();
        this.fetchStats();
    }

    handleDialogOpen = dialog => event =>{
        this.setState({openDialog: `dialog_${dialog}`});
    }

    handleDialogClose = event =>{
        this.setState({openDialog: null});
    }

    handleDelete = item => event =>{
        this.setState({deleting:true});
        api_deleteFoodEntry(item.id,
            (response)=>{
                toast.success('Deleted successfully');
                this.handleDialogClose();
                this.fetchData();
                this.fetchStats();
                this.setState({deleting:false});
            }, (error)=>{
                toast.error('Problem deleting entry');
                this.setState({deleting:false});
            })
    }

    fetchStats = ()=>{
        this.setState({fetching_stats: true})
        api_getFoodEntriesStats(this.props.user.id,
            (response)=>{
                this.setState({
                    stats: response.data?.data?.stats || {},
                    fetching_stats: false
                })
            },
            (error)=>{

            })
    }

    handleOnItemUpdate = ()=>{
        this.setState({openDialog: null})
        this.fetchData();
        this.fetchStats();
    }

    render() {
        const {
            items,
            fetching,

            stats,
            fetching_stats,
        } = this.state;

        const {
            user
        } = this.props;

        return (
            <Grid container spacing={2}>
                <Grid item xs={12} sm={9}>
                    <Card>
                        <CardHeader title={`${user.first_name}'s Food Entries`} titleTypographyProps={{color:"primary"}}/>
                        <TableContainer>
                            <Fade in={fetching}>
                                <div className="absolute top left right">
                                    <LinearProgress />
                                </div>
                            </Fade>
                            {items && items.length>0 ? (
                                <Table size="small">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Food Name</TableCell>
                                            <TableCell>Calories</TableCell>
                                            <TableCell>
                                                <TableSortLabel
                                                    active={this.state.order === 'eaten_at'}
                                                    direction={this.state.order_dir}
                                                    onClick={()=>this.handleSortChange('created_at')}
                                                >
                                                    Eaten at
                                                </TableSortLabel>
                                            </TableCell>
                                            <TableCell>
                                                <TableSortLabel
                                                    active={this.state.order === 'created_at'}
                                                    direction={this.state.order_dir}
                                                    onClick={()=>this.handleSortChange('created_at')}
                                                >
                                                    Added at
                                                </TableSortLabel>
                                            </TableCell>
                                            <TableCell>D-C</TableCell>
                                            <TableCell>Actions</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {items.map((item,ki) => {
                                            return (
                                                <TableRow key={item.id} sx={{backgroundColor: ki%2 !== 1 ? '#fafafa' : 'initial'}} hover>
                                                    <TableCell>
                                                        {item.name}
                                                    </TableCell>
                                                    <TableCell>{item.calories} KCal</TableCell>
                                                    <TableCell><small>{dayjs(item.eaten_at).format('DD/MMM/YYYY hh:mm a')}</small></TableCell>
                                                    <TableCell><small>{dayjs(item.created_at).format('DD/MMM/YYYY hh:mm a')}</small></TableCell>
                                                    <TableCell><Chip label={item.diet_cheating?'CHEAT':'COUNT'} color="secondary" size="small" sx={{opacity: item.diet_cheating ? 1 : 0, mr: 2}}/></TableCell>
                                                    <TableCell sx={{whiteSpace: 'nowrap'}}>

                                                        <IconButton size="small" onClick={this.handleDialogOpen('edit_'+item.id)}><Icon fontSize="small">edit</Icon></IconButton>
                                                        <IconButton size="small" onClick={this.handleDialogOpen('delete_'+item.id)}><Icon fontSize="small" sx={{color: 'error.main'}}>delete</Icon></IconButton>

                                                        <Dialog open={this.state.openDialog === `dialog_edit_${item.id}`}
                                                                onClose={this.handleDialogClose}
                                                                maxWidth="sm"
                                                                fullWidth
                                                        >
                                                            {this.state.openDialog === `dialog_edit_${item.id}` && (
                                                                <UserFoodEntryForm mode="edit" user={user} entryId={item.id} onSuccess={this.handleOnItemUpdate}/>
                                                            )}
                                                        </Dialog>
                                                        <Dialog
                                                            open={this.state.openDialog === `dialog_delete_${item.id}`}
                                                            onClose={this.handleDialogClose}
                                                            maxWidth="xs"
                                                        >
                                                            <DialogTitle>{`Delete entry "${item.name}"?`}</DialogTitle>
                                                            <DialogActions>
                                                                <Button onClick={this.handleDialogClose} color="primary" disabled={this.state.deleting}>
                                                                    No
                                                                </Button>
                                                                <LoadingButton onClick={this.handleDelete(item)} color="primary" variant="contained" autoFocus loading={this.state.deleting}>
                                                                    Yes
                                                                </LoadingButton>
                                                            </DialogActions>
                                                        </Dialog>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })}
                                    </TableBody>
                                    <TableFooter>
                                        <TableRow>
                                            <Pagination
                                                pagination={this.state.pagination}
                                                onPageChange={this.handleChangePage}
                                                onRowsPerPageChange={this.handleChangeRowsPerPage}
                                            />
                                        </TableRow>
                                    </TableFooter>
                                </Table>
                            ):(
                                <Typography variant="overline" component="div" color="text.secondary" sx={{textAlign: 'center', p: 2}}>No entries added yet.</Typography>
                            )}
                        </TableContainer>
                    </Card>
                    <UserFoodEntryForm mode="new" user={user} onSuccess={()=>{this.fetchData(); this.fetchStats()}}/>
                </Grid>
                <Grid item xs={12} sm={3}>
                    {fetching_stats ? (
                        <Box sx={{pt:4,pb: 4, textAlign: 'center'}}>
                            <CircularProgress/>
                        </Box>
                    ):(
                    <React.Fragment>
                        <Card>
                            <CardContent>
                                <Typography component="h2" variant="h5" color="primary" gutterBottom>
                                    User stats
                                </Typography>

                                <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.count || 0} <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Entries</Typography></Typography>
                                <Divider sx={{mt:1, mb:1}}/>
                                <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{stats.total_calories || 0} KCal <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Total Calories</Typography></Typography>
                                <Divider sx={{mt:1, mb:1}}/>
                                <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{Math.round(stats.daily_average) || 0} KCal <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Daily Average</Typography></Typography>
                                <Divider sx={{mt:1, mb:1}}/>
                                <Typography component="p" variant="h4" sx={{display: 'flex', alignItems: 'center'}}>{Math.round(stats.daily_average_last7) || 0} KCal <Typography variant="caption" component="span" color="text.secondary" sx={{ml:1}}>Daily Average (last 7 days)</Typography></Typography>
                            </CardContent>
                        </Card>
                    </React.Fragment>
                    )}
                </Grid>
            </Grid>
        );
    }
}

UserFoodEntriesList.propTypes = {
    user: PropTypes.object
}
export default withRouter(UserFoodEntriesList);
