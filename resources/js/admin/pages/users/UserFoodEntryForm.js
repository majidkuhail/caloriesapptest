import PropTypes from 'prop-types';
import BaseComponent from "../../../common/BaseComponent";
import FormErrors from "../../../form/FormErrors";
import {
    Alert,
    Card,
    CardActions,
    CardContent,
    CardHeader, Checkbox, FormControlLabel, FormGroup,
    Grid,
    InputAdornment,
    Skeleton,
    Stack,
    TextField, Typography
} from "@mui/material";
import React from "react";
import LoadingButton from "@mui/lab/LoadingButton";
import {api_getFoodEntry, api_storeFoodEntry, api_updateFoodEntry} from "../../api";
import {toast} from "react-toastify";
import DateTimePicker from "@mui/lab/DateTimePicker";
import dayjs from "dayjs";


class UserFoodEntryForm extends BaseComponent
{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            calories: '',
            eaten_at: null,
            diet_cheating: false,

            fetching: true,
            fetching_error: null,
            working: false,
            formErrors: new FormErrors()
        }
    }

    componentDidMount() {
        if(this.props.mode === 'edit'){
            this.fetchData();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(
            this.props.mode === 'edit' &&
            this.props.entryId !== prevProps.entryId
        ){
            this.fetchData();
        }
    }

    fetchData = ()=>{
        this.setState({fetching: true})
        api_getFoodEntry(
            this.props.entryId,
            (response)=>{
                const data = response.data?.data || {};
                console.log('data', data);
                this.setState({
                    name: data.name,
                    calories: data.calories,
                    eaten_at: data.eaten_at,
                    diet_cheating: data.diet_cheating,
                    fetching: false,
                })
            },
            (error)=>{
                this.setState({
                    fetching_error: error.response?.data?.message ? error.response.data.message : 'Problem fetching data',
                    fetching: false
                });
            }
        )
    }

    handleSubmit = (event)=>{
        event.preventDefault();

        this.state.formErrors.forget();
        this.setState({ working: true });

        const data = {
            name: this.state.name,
            calories: this.state.calories,
            eaten_at: this.state.eaten_at,
            diet_cheating: this.state.diet_cheating,
        }
        if(this.props.mode === 'edit'){
            //UPDATE ENTRY
            api_updateFoodEntry(this.props.entryId, data, (response)=>{
                toast.success("Changes saved.");
                this.setState({
                    working: false,
                })
                if(this.props.onSuccess){
                    this.props.onSuccess();
                }
            }, (error)=>{
                this.state.formErrors.processResponse(
                    error.response,
                    'Please check your inputs'
                );
                this.setState({ working: false });
            })
        }else{
            //STORE NEW ENTRY
            api_storeFoodEntry(this.props.user.id, data, (response)=>{
                toast.success("Entry added successfully.");
                this.setState({
                    name: '',
                    calories: '',
                    eaten_at: null,
                    diet_cheating: false,
                    working: false,
                })
                if(this.props.onSuccess){
                    this.props.onSuccess();
                }
            }, (error)=>{
                this.state.formErrors.processResponse(
                    error.response,
                    'Please check your inputs'
                );
                this.setState({ working: false });
            })
        }
    }

    render() {
        const {
            name,
            calories,
            eaten_at,
            diet_cheating,

            fetching,
            fetching_error,
            working,
            formErrors
        } = this.state;

        const isEditMode = (this.props.mode === 'edit')
        if(isEditMode) {
            if(fetching) {
                return (
                    <Card>
                        <CardHeader title={<Skeleton animation="wave" height={30} width="50%"/>}/>
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item sm={6}>
                                    <Skeleton animation="wave" sx={{margin:0}} height={60} width="100%"/>
                                </Grid>
                                <Grid item sm={6}>
                                    <Skeleton animation="wave" sx={{margin:0}} height={60} width="100%"/>
                                </Grid>
                                <Grid item sm={8}>
                                    <Skeleton animation="wave" sx={{margin:0}} height={60} width="100%"/>
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions>
                            <Stack width="100%" direction="row" spacing={2} justifyContent="flex-end">
                                <Skeleton animation="wave" sx={{margin:0}} height={60} width="100px"/>
                            </Stack>
                        </CardActions>
                    </Card>
                )
            }
            if(fetching_error){
                return (
                    <Card>
                        <Alert severity="warning">{fetching_error}</Alert>
                    </Card>
                )
            }
        }
        return (
            <form onSubmit={this.handleSubmit}>
                <Card>
                    <CardHeader title={isEditMode ? `Edit: ${name}` : 'Add New Entry'} titleTypographyProps={{variant: "h4", color:"primary"}}/>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item sm={6}>
                                <TextField
                                    label="Name"
                                    value={name}
                                    margin="none"
                                    placeholder="Food name"
                                    size="small"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('name')}
                                    error={formErrors.has('name')}
                                    helperText={formErrors.get('name')}
                                    // required
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TextField
                                    label="Calories"
                                    value={calories}
                                    margin="none"
                                    type="number"
                                    size="small"
                                    min="0"
                                    fullWidth
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">KCal</InputAdornment>
                                    }}
                                    disabled={working}
                                    onChange={this.handleInputChange('calories')}
                                    error={formErrors.has('calories')}
                                    helperText={formErrors.get('calories')}
                                    // required
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <DateTimePicker
                                    label="Time"
                                    ampm={true}
                                    inputFormat="DD/MM/YYYY hh:mm a"
                                    renderInput={(params) => <TextField size="small" margin="none" helperText={formErrors.get('eaten_at')} error={formErrors.has('eaten_at')} {...params} />}
                                    value={eaten_at}
                                    onChange={this.handleInputValueChange('eaten_at')}
                                    disabled={working}
                                    maxDate={dayjs().endOf('day')}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <FormGroup>
                                    <FormControlLabel
                                        control={<Checkbox checked={diet_cheating} onClick={this.handleCheckboxChange('diet_cheating')} />}
                                        label="Diet-Cheating" />
                                    <Typography variant="caption" color="text.secondary">Excludes this entry's calories</Typography>
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Stack width="100%" direction="row" spacing={2} justifyContent="flex-end">
                            <LoadingButton
                                variant="contained"
                                type="submit"
                                color="primary"
                                loading={working}
                                size="medium"
                            >
                                {isEditMode ? 'Save Changes' : 'Add Entry' }
                            </LoadingButton>
                        </Stack>
                    </CardActions>
                </Card>
            </form>
        );
    }

}

UserFoodEntryForm.propTypes = {
    mode: PropTypes.oneOf(['new', 'edit']),
    entryId: PropTypes.string,
    user: PropTypes.object,
    onSuccess: PropTypes.func
}

export default UserFoodEntryForm;
