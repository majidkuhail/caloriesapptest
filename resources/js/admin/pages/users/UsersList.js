import React from "react";
import {Link, withRouter} from "react-router-dom";
import {
    Card,
    CardContent,
    Grid,
    Skeleton,
    Typography,
    Divider,
    Container,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableSortLabel,
    TableBody,
    TableContainer,
    Paper,
    Fade,
    LinearProgress,
    Avatar,
    IconButton,
    Icon, Dialog, DialogTitle, DialogActions, Button, TableFooter
} from "@mui/material";
import {api_deleteUser, api_getUsers} from "../../api";
import {toast} from "react-toastify";
import PaginateComponent from "../../common/PaginateComponent";
import _merge from "lodash/merge";
import {adminApiUrl} from "../../utils";
import Endpoints from "../../api/Endpoints";
import routes from "../../routes/routes";
import LoadingButton from "@mui/lab/LoadingButton";
import Pagination from "../../common/Pagination";
import {routeAssignParams} from "../../../utils";

class UsersList extends PaginateComponent
{
    constructor(props) {
        super(props);
        this.state = _merge(this.state,{
            url: adminApiUrl(Endpoints.USERS),
            order: 'created_at',
            order_dir: 'desc',
            parameters: [],

            filters:{

            },

            openDialog: null,
            deleting: false,
        });
    }

    componentDidMount() {
        this.fetchData();
    }

    handleDialogOpen = dialog => event =>{
        this.setState({openDialog: `dialog_${dialog}`});
    }

    handleDialogClose = event =>{
        this.setState({openDialog: null});
    }

    handleDelete = item => event =>{
        this.setState({deleting:true});
        api_deleteUser(item.id,
            (response)=>{
                toast.success('Deleted successfully');
                this.handleDialogClose();
                this.fetchData();
                this.setState({deleting:false});
            }, (error)=>{
                toast.error('Problem deleting user');
                this.setState({deleting:false});
            })
    }

    render() {
        const {
            items,
            fetching
        } = this.state;
        return (
            <Container maxWidth="md">
                <Typography variant="h2" component="h1" sx={{mb: 3}}>Users List</Typography>

                <TableContainer component={Paper}>
                    <Fade in={fetching}>
                        <div className="absolute top left right">
                            <LinearProgress />
                        </div>
                    </Fade>
                    {items && items.length>0 &&(
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <TableSortLabel
                                            active={this.state.order === 'first_name'}
                                            direction={this.state.order_dir}
                                            onClick={()=>this.handleSortChange('first_name')}
                                        >
                                            Name
                                        </TableSortLabel>
                                    </TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>Calories Limit</TableCell>
                                    <TableCell>
                                        <TableSortLabel
                                            active={this.state.order === 'created_at'}
                                            direction={this.state.order_dir}
                                            onClick={()=>this.handleSortChange('created_at')}
                                        >
                                            Created at
                                        </TableSortLabel>
                                    </TableCell>
                                    <TableCell>Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody hidden={this.state.working}>
                                {this.state.items.map(item => {
                                    return (
                                        <TableRow key={item.id} hover>
                                            <TableCell sx={{whiteSpace: 'nowrap'}}>
                                                  <Avatar sx={{display: 'inline-flex', mr:1, bgcolor:'primary.main'}}>{item.first_name.charAt(0)+item.last_name.charAt(0)}</Avatar>
                                                {item.name}
                                            </TableCell>
                                            <TableCell>{item.email}</TableCell>
                                            <TableCell>{item.calories_limit} KCal</TableCell>
                                            <TableCell><small>{item.created_at}</small></TableCell>
                                            <TableCell sx={{whiteSpace: 'nowrap'}}>

                                                <IconButton size="small" component={Link} to={routeAssignParams(routes.edit_user, {id: item.id})}><Icon fontSize="small">edit</Icon></IconButton>
                                                <IconButton size="small" onClick={this.handleDialogOpen(item.id)}><Icon fontSize="small" sx={{color: 'error.main'}}>delete</Icon></IconButton>

                                                <Dialog
                                                    open={this.state.openDialog === `dialog_${item.id}`}
                                                    onClose={this.handleDialogClose}
                                                    maxWidth="xs"
                                                >
                                                    <DialogTitle>{`Delete user "${item.first_name}"?`}</DialogTitle>
                                                    <DialogActions>
                                                        <Button onClick={this.handleDialogClose} color="primary" disabled={this.state.deleting}>
                                                            No
                                                        </Button>
                                                        <LoadingButton onClick={this.handleDelete(item)} color="primary" variant="contained" autoFocus loading={this.state.deleting}>
                                                            Yes
                                                        </LoadingButton>
                                                    </DialogActions>
                                                </Dialog>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <Pagination
                                        pagination={this.state.pagination}
                                        onPageChange={this.handleChangePage}
                                        onRowsPerPageChange={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    )}
                </TableContainer>
            </Container>
        );
    }
}

export default withRouter(UsersList);
