import {adminRoutePath} from "../utils";
import Dashboard from "../pages/Dashboard";
import UsersList from "../pages/users/UsersList";
import EditUser from "../pages/users/EditUser";

export default {
    dashboard: {
        path: adminRoutePath(''),
        exact: true,
        auth: true,
        component: Dashboard,
        title: 'Dashboard'
    },
    users: {
        path: adminRoutePath('users'),
        exact: true,
        auth: true,
        component: UsersList,
        title: 'Users List'
    },
    edit_user: {
        path: adminRoutePath('users/:id'),
        exact: true,
        auth: true,
        component: EditUser,
        title: 'Edit User'
    }
}
