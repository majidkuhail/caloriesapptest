import {ADMIN_API_URL} from "../../settings";

export function adminRoutePath(route) {
    const path = typeof route === "string" ? route : route.path;
    return `/admin${path.charAt(0) === "/" ? "" : "/"  }${path}`;
}

export function adminApiUrl(path) {
    return ADMIN_API_URL + (path.charAt(0) === "/" ? "" : "/") + path;
}
