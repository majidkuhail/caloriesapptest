import Cookies from 'universal-cookie';
import {ADMIN_URL, API_URL, CLIENT_ID, CLIENT_SECRET, OAUTH_URL, TOKEN_COOKIE} from "../settings";
import authStore from "../stores/authStore";
import Endpoints from "./Endpoints";
import {apiUrl} from "../utils";

const cookies = new Cookies();

export const initAuth = function(callback) {
    const token_cookie = cookies.get(TOKEN_COOKIE, { path: '/' });
    if(token_cookie && token_cookie !== ''){
        authStore.setToken(token_cookie);
        setAxiosDefaults(token_cookie);
        fetchCurrentUser(callback);
    }else{
        setAxiosDefaults();
        if(callback) {
            callback();
        }
    }
}

export const setAxiosDefaults = function(token){
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    if(token) {
        axios.defaults.headers.common.Authorization = token;
    }
}

export const login = function({username,password,remember}, callback, error_callback) {
    const data = {
        username,
        password,
        grant_type: 'password',
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET
    }

    axios.post(`${OAUTH_URL}/token`, data)
        .then(response => {
            const token = `${response.data.token_type  } ${  response.data.access_token}`;
            cookies.set(TOKEN_COOKIE, token, { path: '/' });
            setAxiosDefaults(token);
            authStore.setToken(response.data);
            if(callback){
                callback(token)
            }
        })
        .catch(error=>{
            if(error_callback){
                error_callback(error)
            }
            console.error(error);
        })
}

export const logout = function() {
    cookies.remove(TOKEN_COOKIE, { path: '/' });
    authStore.forgetToken();
}

export const fetchCurrentUser = function(callback) {
    axios.get(apiUrl(Endpoints.USER))
        .then(response => {
            const user = response.data.data;
            authStore.setUser(user);
            if(callback){
                callback(user)
            }
        })
        .catch(error=>{
            console.log(error);
            logout();
        })
}
