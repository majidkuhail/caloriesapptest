const Endpoints = {
    USER: "/user",
    REGISTER: "/user/register",
    CALORIES_LIMIT: "/user/calories_limit",

    FOOD_ENTRIES: "/food_entries",
};
export default Endpoints;
