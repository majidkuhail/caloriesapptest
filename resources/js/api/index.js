import Endpoints from "./Endpoints";
import authStore from "../stores/authStore";
import {logout} from "./Auth";
import {apiUrl} from "../utils";

export function api_registerUser(data, success, error) {
    axios({
            url: apiUrl(Endpoints.REGISTER),
            method: 'POST',
            data: data,
            contentType: "application/json"
        })
        .then(success)
        .catch(error);
}

export function api_getFoodEntries(success, error) {
    axios.get(apiUrl(Endpoints.FOOD_ENTRIES))
        .then(success)
        .catch(error);
}

export function api_getFoodEntry(entryId, success, error) {
    axios.get(apiUrl(Endpoints.FOOD_ENTRIES + '/' + entryId))
        .then(success)
        .catch(error);
}

export function api_storeFoodEntry(data, success, error) {
    axios({
           url: apiUrl(Endpoints.FOOD_ENTRIES),
            method: 'POST',
            data: data,
            contentType: "application/json"
        })
        .then(success)
        .catch(error);
}

export function api_updateFoodEntry(entryId, data, success, error) {
    axios({
        url: apiUrl(Endpoints.FOOD_ENTRIES + '/' + entryId),
        method: 'PUT',
        data: data,
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}

export function api_deleteFoodEntry(entryId, success, error) {
    axios({
        url: apiUrl(Endpoints.FOOD_ENTRIES + '/' + entryId),
        method: 'DELETE',
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}

export function api_updateCaloriesLimit(data, success, error) {
    axios({
        url: apiUrl(Endpoints.CALORIES_LIMIT),
        method: 'PUT',
        data: data,
        contentType: "application/json"
    })
        .then(success)
        .catch(error);
}
