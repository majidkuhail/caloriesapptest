import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from "@mui/material/styles";
import { Provider } from 'mobx-react';
import { createBrowserHistory } from 'history';
import authStore from './stores/authStore';
import { getElementAttributes } from './utils';
import {ADMIN_URL, API_URL} from './settings';
import { initAuth } from './api/Auth';
import Theme from "./theme/default";
import {LinearProgress} from "@mui/material";
import Routes from "./routes";
import AdapterDatejs from '@mui/lab/AdapterDayjs';
import LocalizationProvider from '@mui/lab/LocalizationProvider';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');
axios.defaults.baseURL = API_URL;

/**
 * common global variables for MOBX stores and history
 */
const history = createBrowserHistory();
const stores = {
    authStore
};
// For easier debugging
if (process.env.NODE_ENV !== 'production') {
    window._____APP_STATE_____ = stores;
}

/**
 * Main app component
 */
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authReady: false
        };
    }

    componentDidMount() {
        initAuth(() => {
            if (stores.authStore.isAdmin) {
                location.href = ADMIN_URL;
                return
            }
            this.setState({ authReady: true });
        });
    }

    render() {
        if (!this.state.authReady) {
            return (
                <ThemeProvider theme={Theme}>
                    <LinearProgress />
                </ThemeProvider>
            );
        }
        return (
            <Provider {...stores}>
                <ThemeProvider theme={Theme}>
                    <LocalizationProvider dateAdapter={AdapterDatejs}>
                        <Routes history={history} />
                    </LocalizationProvider>
                </ThemeProvider>
            </Provider>
        );
    }
}

/**
 * Render React components in dom
 */
const AppComponents = [
    { id: 'App', component: App }
];

for (const k in AppComponents) {
    const { id, component: Comp } = AppComponents[k];
    const Elem = document.querySelector(`#${id}`);
    if (Elem) {
        ReactDOM.render(
            <Comp
                {...stores}
                {...getElementAttributes(Elem)}
                history={history}
            />,
            Elem
        );
    }
}
