import React from "react";
import FormErrors from "../form/FormErrors";

export default class BaseComponent extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            formErrors: null
        }
    }

    handleInputChange = name => event => {
        this.setState({ [name]: event.target.value });
        if(this.formErrors){
            this.formErrors.forget(name);
        }
    }

    handleInputValueChange = name => value => {
        this.setState({ [name]: value });
        if(this.formErrors){
            this.formErrors.forget(name);
        }
    }

    handleCheckboxChange = name => event => {
        this.setState({ [name]: event.target.checked });
        if(this.formErrors){
            this.formErrors.forget(name);
        }
    }
}
