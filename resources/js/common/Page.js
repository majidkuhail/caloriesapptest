import React from "react"
import {updatePageTitle} from "../utils";

class Page extends React.Component {

    componentDidMount() {
        updatePageTitle(this.props.title)
    }

    render() {
        const {
            location,
            props
        } = this.props;
        const PageComponent = this.props.component;

        return (
            <main className={`page page-${props.location?.pathname?.replaceAll('/','-')}`}>
                <PageComponent {...props} />
            </main>
        )
    }
}

export default Page
