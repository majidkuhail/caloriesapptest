import React from "react";
import {AppBar, Button, CssBaseline, Icon, Toolbar, Typography} from "@mui/material";
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import routes from "../routes/routes";
import {getUrl} from "../utils";
import {logout as AuthLogout} from "../api/Auth";

@inject('authStore')
@observer
class Header extends React.Component
{
    handleLogout = () => {
        AuthLogout();
        setTimeout(() => {
            location.href = getUrl(routes.login.path);
        }, 300);
    }

    render() {
        const {
            authStore
        } = this.props;
        const auth = authStore && authStore.checkUser
        const user = auth ? authStore.user : null;
        return (
            <React.Fragment>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h2" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                            Calories App
                        </Typography>
                        {auth && (
                            <React.Fragment>
                                <Button variant="text" color="inherit" startIcon={<Icon>account_circle</Icon>}>
                                    {user.name}
                                </Button>
                                <Button color="inherit" variant="outlined" size="small" onClick={this.handleLogout}>Sign out</Button>
                            </React.Fragment>
                        )}
                    </Toolbar>
                </AppBar>
            </React.Fragment>
        );
    }
}

export default withRouter(Header);
