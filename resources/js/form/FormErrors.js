import { toast } from 'react-toastify';

class FormErrors  {
    errors= {}

    // Set the raw errors for the collection.
    set (errors) {
        // console.log(errors);
        if (typeof errors === 'object') {
            this.errors = errors;
        } else if (typeof errors === 'string') {
            this.errors = {'field': [errors]};
        } else {
            this.errors = {'field': ['Something went wrong. Please try again.']};
        }
    }

    processResponse(response, alert){
        if(!response){
            toast.error("Something went wrong, Please try again")
        }
        const errors = response.data?.errors || response.data?.data?.errors
        if(response.status === 422 && errors) {
            this.set(errors);
            if(alert){
                toast.error(alert)
            }
        }else if(response.status === 403) {
            toast.error("Unauthorised request")
        }else if(response.data?.message) {
            toast.error(response.data.message)
        }
    }


    // Determine if the collection has any errors.
    // hasErrors () {
    //   return ! _.isEmpty(this.errors);
    // },


    // Determine if the collection has errors for a given field.
    has (field) {
        return Object.keys(this.errors).indexOf(field) > -1;
    }

    hasErrors(){
        return Object.keys(this.errors).length > 0;
    }


    // Get all of the raw errors for the collection.
    all () {
        return this.errors;
    }


    // Get all of the errors for the collection in a flat array.
    // return _.flatten(_.toArray(this.errors));
    flatten (object, separator = '.') {
        return Object.assign({}, ...function _flatten(child, path = []) {
            return [].concat(...Object.keys(child).map(key => typeof child[key] === 'object'
                ? _flatten(child[key], path.concat([key]))
                : ({ [path.concat([key]).join(separator)] : child[key] })
            ));
        }(object));
    }


    // Get the first error message for a given field.
    get (field) {
        if (this.has(field)) {
            // console.log(this.errors[field][0]);
            return this.errors[field][0];
        }
    }


    // Forget all of the errors currently in the collection.
    forget (field) {
        if(field === undefined || typeof field === 'undefined' || field === null || field === false){
            this.errors = {};
        }else if(this.errors.hasOwnProperty(field)){
            delete this.errors[field];
        }
    }

    append(field, error){
        let errorArray = []
        if(this.errors.hasOwnProperty(field)){
            errorArray = this.errors[field];
        }
        if (typeof error === 'string') {
            errorArray.push(error);
        }else if(typeof error === 'array') {
            errorArray = error;
        }
        this.errors[field] = errorArray;
    }
}

export default FormErrors;
