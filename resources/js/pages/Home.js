import React from "react";
import BaseComponent from "../common/BaseComponent";
import {withRouter} from "react-router-dom";
import {
    Accordion, AccordionDetails, AccordionSummary, Alert,
    Avatar, Box, Button, Chip,
    Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Icon,
    IconButton, LinearProgress,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText, ListSubheader, Skeleton, Stack, TextField, Typography
} from "@mui/material";
import EntryForm from "./Home/EntryForm";
import {api_deleteFoodEntry, api_getFoodEntries} from "../api";
import _map from "lodash/map";
import _filter from "lodash/filter";
import dayjs from "dayjs";
import EditCaloriesLimitForm from "./Home/EditCaloriesLimitForm";
import {inject, observer} from "mobx-react";
import {toast} from "react-toastify";
import LoadingButton from "@mui/lab/LoadingButton";
import DateRangePicker from "@mui/lab/DateRangePicker";

@inject('authStore')
@observer
class Home extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            fetching: true,
            fetching_error: null,

            open_edit: null,
            open_delete: null,
            deleting: null,

            filter_day: null,
            filter_day_range: [null, null],
        }
    }

    componentDidMount() {
        this.fetchItems();
    }

    fetchItems(){
        this.setState({fetching: true});
        api_getFoodEntries(
            (response)=>{
                const data = response.data;
                console.log(data);
                this.setState({
                    items: data.data,
                    fetching: false
                });
            },
            (error)=>{
                this.setState({
                    fetching_error: error.response?.data?.message || 'Problem fetching your food entries',
                    fetching: false
                });
            });
    }

    handleLimitUpdated = ()=>{
        this.forceUpdate();
    }

    handleOpenEditDialog = id => event =>{
        this.setState({
            open_edit: id,
        });
    }

    handleOpenDeleteDialog = id => event =>{
        this.setState({
            open_delete: id,
        });
    }

    handleCloseDialog = ()=>{
        this.setState({
            open_edit: null,
            open_delete: null
        })
    }

    handleDelete = itemId => event => {
        event.preventDefault()
        this.setState({deleting: itemId})
        api_deleteFoodEntry(itemId, (response)=>{
            this.setState({open_delete: null, deleting: null})
            this.fetchItems();
            toast.success('Deleted successfully')
        }, (error)=>{
            console.log('error',error);
            toast.error(error?.response?.data?.message || 'Problem deleting entry.')
        });
    }

    handleOnItemUpdate = ()=>{
        this.setState({open_edit: null})
        this.fetchItems();
    }

    handleResetRangeFilter = ()=>{
        this.setState({
            filter_day_range: [null,null]
        })
    }

    render() {
        const {
            items,
            fetching,
            fetching_error,

            filter_day,
            filter_day_range
        } = this.state;

        const caloriesLimit = this.props.authStore?.getUser?.calories_limit || 2100;

        let groupedItems = [];
        if(items && items.length){
            let groupedDateKeyMap = [];
            let keyIrt = 0;
            _map(items, (item, k)=>{
                const itemDayJs = dayjs(item.eaten_at);
                const itemDay = itemDayJs.format('ddd - D/MMM/YYYY')
                const itemDayKey = itemDayJs.format('DDMMYYYY');
                const index = groupedDateKeyMap.indexOf(itemDayKey)
                if(index >= 0){
                    groupedItems[index].items.push(item)
                }else{
                    groupedDateKeyMap[keyIrt] = itemDayKey
                    groupedItems[keyIrt] = {
                        label: itemDay,
                        dayKey: itemDayKey,
                        day: itemDayJs.format('YYYY-MM-DD'),
                        items: [item],
                    };
                    keyIrt++
                }
            })
        }

        const hasRangeFilter = filter_day_range && filter_day_range[0] && filter_day_range[1];
        if(hasRangeFilter){
            console.log('filter_day_range', filter_day_range)
            groupedItems = _filter(groupedItems, (group)=>{
                const groupDayjs  = dayjs(group.day);
                if(
                    !filter_day_range[0] &&
                    filter_day_range[1] &&
                    groupDayjs.isSameOrAfter(filter_day_range[0])
                ){
                    return true;
                } else if (
                    filter_day_range[0] &&
                    !filter_day_range[1] &&
                    groupDayjs.isSameOrBefore(filter_day_range[1])
                ){
                    return true;
                }else if(
                    filter_day_range[0] &&
                    filter_day_range[1] &&
                    groupDayjs.isBetween(filter_day_range[0], filter_day_range[1], null, '[]')
                ){
                    return true;
                }
                return false;
            });
        }

        return (
            <Container maxWidth="sm">
                <EntryForm mode="new" onSuccess={()=>{this.fetchItems()}}/>

                <EditCaloriesLimitForm onUpdated={this.handleLimitUpdated}/>

                <Stack sx={{mb:2,mt:3}} width="100%" direction="row" alignItems="center">
                    <Typography sx={{flexGrow: 1}} variant="h2" component="h2">Your Entries</Typography>
                    {hasRangeFilter && (
                        <IconButton onClick={this.handleResetRangeFilter}><Icon>close</Icon></IconButton>
                    )}
                    <DateRangePicker
                        label="Search by date range"
                        startText="From"
                        endText="To"
                        value={filter_day_range}
                        inputFormat="DD/MM/YYYY"
                        inputVariant="outlined"
                        variant="inline"
                        clearable
                        onChange={this.handleInputValueChange('filter_day_range')}
                        renderInput={(startProps, endProps) => (
                            <React.Fragment>
                                <TextField size="small" margin="none" sx={{width:100}} {...startProps} />
                                <Box sx={{ mx: 1 }}> to </Box>
                                <TextField size="small" margin="none" sx={{width:100}} {...endProps} />
                            </React.Fragment>
                        )}
                    />
                </Stack>

                {fetching ? (
                    <React.Fragment>
                        {_map(new Array(8), (a,k)=>(
                        <Accordion key={k}>
                            <AccordionSummary><Skeleton animation="wave" height={20} width="25%"/></AccordionSummary>
                        </Accordion>
                        ))}
                    </React.Fragment>
                ): fetching_error ? (
                    <Alert severity="error">{fetching_error}</Alert>
                ): groupedItems.length === 0 ? (
                    <div>
                        {filter_day && filter_day!=='' ? (
                            <Alert severity="info">No entries found for selected date.</Alert>
                        ):(
                            <Alert severity="info">You have no entries yet! <br/>Start adding using the form above</Alert>
                        )}
                    </div>
                ):(
                    <React.Fragment>
                    {_map(groupedItems, (group, key)=>{
                        let groupTotalCals = 0;
                        _map(group.items, (item) => {
                            if(item.diet_cheating){ return; }
                            groupTotalCals+= item.calories;
                        });
                        const caloriesPercentage = groupTotalCals<caloriesLimit ? (100 * groupTotalCals/caloriesLimit) : 100;
                        return(
                            <Accordion key={key} defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<Icon>expand_more</Icon>}
                                    aria-controls="panel-content"
                                    id={`panel${key}-header`}
                                >
                                    <Stack direction="row" width="100%" spacing={4} sx={{paddingRight: 1}}>
                                        <Typography sx={{flexGrow: 1}}>{group.label}</Typography>
                                        <Box sx={{ width: 120 }}>
                                            <Typography variant="caption" component="span"
                                                        color={groupTotalCals>caloriesLimit ? 'secondary.main' : 'success.main'}>
                                                {`${groupTotalCals} `}
                                            </Typography>
                                            <Typography variant="caption" component="span" color="text.secondary">
                                                {`/ ${caloriesLimit} KCal`}
                                            </Typography>
                                            <LinearProgress
                                                variant="determinate"
                                                value={caloriesPercentage}
                                                color={
                                                    groupTotalCals>caloriesLimit ?
                                                        'error'
                                                    :
                                                        'success'
                                                } />
                                        </Box>
                                    </Stack>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List sx={{ width: '100%'}}>
                                    {_map(group.items, (item, k)=>(
                                        <ListItem
                                            key={k}
                                            dense
                                            divider={k !== group.items.length-1}
                                        >
                                            <ListItemAvatar>
                                                <Avatar sx={{ bgcolor: '#'+Math.floor((((k%20)+50)/70)*16777215).toString(16) }}>
                                                    <Icon>fastfood_outlined</Icon>
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={item.name} secondary={dayjs(item.eaten_at).format('h:mm a')} />
                                            <ListItemText primary={item.calories} secondary="KCal" />

                                            <Chip label={item.diet_cheating?'CHEAT':'COUNT'} color="secondary" size="small" sx={{opacity: item.diet_cheating ? 1 : 0, mr: 2}}/>


                                            <IconButton edge="start" aria-label="edit" onClick={this.handleOpenEditDialog(item.id)}>
                                                <Icon>edit</Icon>
                                            </IconButton>
                                            <IconButton edge="end" color="default" aria-label="delete" onClick={this.handleOpenDeleteDialog(item.id)} sx={{color: 'error.main'}}>
                                                <Icon>delete</Icon>
                                            </IconButton>

                                            <Dialog open={this.state.open_edit === item.id}
                                                    onClose={this.handleCloseDialog}
                                                    maxWidth="sm"
                                                    fullWidth
                                            >
                                                {this.state.open_edit === item.id && (
                                                    <EntryForm mode="edit" entryId={item.id} onSuccess={this.handleOnItemUpdate}/>
                                                )}
                                            </Dialog>
                                            <Dialog open={this.state.open_delete === item.id}
                                                    onClose={this.handleCloseDialog}
                                                    maxWidth="xs"
                                                    fullWidth
                                            >
                                                <DialogTitle>Delete "{item.name}" Entry?</DialogTitle>
                                                <DialogActions>
                                                    <Button onClick={this.handleCloseDialog}>Cancel</Button>
                                                    <LoadingButton variant="contained" color="primary" loading={this.state.deleting===item.id} onClick={this.handleDelete(item.id)}>Delete</LoadingButton>
                                                </DialogActions>
                                            </Dialog>
                                        </ListItem>
                                    ))}
                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        )
                    })}
                    </React.Fragment>
                )}
            </Container>
        );
    }
}

export default withRouter(Home);
