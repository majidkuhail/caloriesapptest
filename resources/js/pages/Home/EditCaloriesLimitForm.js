import PropTypes from 'prop-types';
import BaseComponent from "../../common/BaseComponent";
import FormErrors from "../../form/FormErrors";
import {
    Button,
    Card,
    CardContent,
    CardHeader,
    InputAdornment,
    Stack,
    TextField, Typography
} from "@mui/material";
import React from "react";
import LoadingButton from "@mui/lab/LoadingButton";
import {toast} from "react-toastify";
import {fetchCurrentUser} from "../../api/Auth";
import {api_updateCaloriesLimit} from "../../api";
import {inject, observer} from "mobx-react";

@inject('authStore')
@observer
class EditCaloriesLimitForm extends BaseComponent
{
    constructor(props) {
        super(props);
        this.state = {
            calories_limit: this.props.authStore.getUser.calories_limit || 2100,

            working: false,
            formErrors: new FormErrors()
        }
    }

    handleSubmit = (event)=>{
        event.preventDefault();

        this.state.formErrors.forget();
        this.setState({ working: true });

        const data = {
            calories_limit: this.state.calories_limit
        }
        api_updateCaloriesLimit(data, (response)=>{
            toast.success("Changes saved.");
            fetchCurrentUser(()=>{
                this.setState({
                    working: false,
                })
                this.handleFromReset();
                if(this.props.onUpdated){
                    this.props.onUpdated();
                }
            });
        }, (error)=>{
            this.state.formErrors.processResponse(
                error.response,
                'Please check your inputs'
            );
            this.setState({ working: false });
        })
    }

    handleFromReset = ()=>{
        this.setState({calories_limit: this.props.authStore.getUser?.calories_limit})
    }

    render() {
        const {
            calories_limit,

            working,
            formErrors
        } = this.state;

        const formChanged = calories_limit !== this.props.authStore.getUser?.calories_limit;
        return (
            <form onSubmit={this.handleSubmit}>
                <Card>
                    <CardContent style={{paddingBottom: 16}}>
                        <Stack direction="row" width="100%" spacing={2} alignItems="center">
                            <Typography variant="h5" component="h4" sx={{whiteSpace: 'nowrap'}}>Your daily calories limit:</Typography>
                            <div>
                                <TextField
                                    label="Calories"
                                    value={calories_limit}
                                    margin="none"
                                    type="number"
                                    size="small"
                                    min="0"
                                    fullWidth
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">KCal</InputAdornment>
                                    }}
                                    disabled={working}
                                    onChange={this.handleInputChange('calories_limit')}
                                    error={formErrors.has('calories_limit')}
                                    helperText={formErrors.get('calories_limit')}
                                    // required
                                />
                            </div>
                            <LoadingButton
                                variant="contained"
                                type="submit"
                                color="primary"
                                loading={working}
                                size="medium"
                                disabled={!formChanged}
                            >
                                Update
                            </LoadingButton>
                            {formChanged && (
                                <Button variant="outlined" onClick={this.handleFromReset}>Cancel</Button>
                            )}
                        </Stack>
                    </CardContent>
                </Card>
            </form>
        );
    }

}

EditCaloriesLimitForm.propTypes = {
    onUpdated: PropTypes.func
}

export default EditCaloriesLimitForm;
