import React from "react";
import BaseComponent from "../common/BaseComponent";
import {withRouter} from "react-router-dom";
import {Button, Card, CardActions, CardContent, Container, Grid, Paper, Typography} from "@mui/material";
import LoginForm from "./Login/LoginForm";
import RegisterForm from "./Login/RegisterForm";

class Login extends BaseComponent {
    render() {
        return (
            <Container maxWidth="md">
                <Grid container spacing={6}>
                    <Grid item xs={12} sm={5}>
                        <LoginForm/>
                    </Grid>
                    <Grid item xs={12} sm={7}>
                        <RegisterForm/>
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

export default withRouter(Login);
