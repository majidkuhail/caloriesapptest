import React from "react";
import BaseComponent from "../../common/BaseComponent";
import {Button, Card, CardActions, CardContent, CardHeader, Stack, TextField, Typography} from "@mui/material";
import {fetchCurrentUser, login} from "../../api/Auth";
import {toast} from "react-toastify";
import {ADMIN_URL} from "../../settings";
import LoadingButton from "@mui/lab/LoadingButton";
import {withRouter} from "react-router-dom";
import routes from "../../routes/routes";

class LoginForm extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            username: 'majid@example.org',
            password: 'password',

            working: false,
            error_username: null,
            error_password: null
        };
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let hasErrors = false;
        if (this.state.username === '') {
            this.setState({ error_username: 'Please enter your email' });
            hasErrors = true;
        }
        if (this.state.password === '') {
            this.setState({ error_password: 'Please enter your password' });
            hasErrors = true;
        }
        if (hasErrors) {
            return;
        }
        this.setState({ working: true });
        login(
            {
                username: this.state.username,
                password: this.state.password
            },
            (token) => {
                fetchCurrentUser((user) => {
                    // this.setState({ working: false });
                    if (user?.role === 'admin') {
                        location.href = ADMIN_URL;
                    } else if (this.state.redirect) {
                        location.href = this.state.redirect;
                    } else {
                        this.props.history.push(routes.home.path);
                    }
                });
            },
            (error) => {
                this.setState({ password: '', working: false });
                toast.error('Wrong Credentials, Please try again');
            }
        );
        return false;
    }

    render() {
        const {
            username,
            password,

            working,
            error_username,
            error_password
        } = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <Card variant="outlined" elevation={0}>
                    <CardHeader title="Login"/>
                    <CardContent>
                        <p className="tac">
                            Please enter your details below to login
                        </p>
                        <TextField
                            label="Email Address"
                            value={username}
                            margin="normal"
                            size="small"
                            placeholder="name@domain.com"
                            fullWidth
                            InputProps={{
                                inputProps: { autoComplete: 'off' }
                            }}
                            autoFocus
                            disabled={working}
                            onChange={this.handleInputChange('username')}
                            error={error_username && error_username !== ''}
                            helperText={error_username}
                        />
                        <TextField
                            type="password"
                            label="Password"
                            value={password}
                            margin="normal"
                            size="small"
                            placeholder="******"
                            fullWidth
                            InputProps={{
                                inputProps: { autoComplete: 'off' }
                            }}
                            disabled={working}
                            onChange={this.handleInputChange('password')}
                            error={error_password && error_password !== ''}
                            helperText={error_password}
                        />
                    </CardContent>
                    <CardActions>
                        <Stack
                            width="100%"
                            direction="row"
                            justifyContent="flex-end"
                            alignItems="center"
                            spacing={2}
                        >
                            <LoadingButton
                                variant="contained"
                                type="submit"
                                color="primary"
                                loading={working}
                                size="large"
                            >
                                Login
                            </LoadingButton>
                        </Stack>
                    </CardActions>
                </Card>
            </form>
        )
    }
}

export default withRouter(LoginForm);
