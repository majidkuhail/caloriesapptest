import React from "react";
import BaseComponent from "../../common/BaseComponent";
import {Button, Card, CardActions, CardContent, CardHeader, Grid, Stack, TextField, Typography} from "@mui/material";
import {fetchCurrentUser, login} from "../../api/Auth";
import {toast} from "react-toastify";
import {ADMIN_URL} from "../../settings";
import LoadingButton from "@mui/lab/LoadingButton";
import {withRouter} from "react-router-dom";
import routes from "../../routes/routes";
import FormErrors from "../../form/FormErrors";
import {api_registerUser} from "../../api";
import {appUrl} from "../../utils";

class RegisterForm extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            password_confirmation: '',

            working: false,
            formErrors: new FormErrors()
        };
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.working) {
            return;
        }
        this.state.formErrors.forget();
        this.setState({ working: true });

        const data = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation
        }

        api_registerUser(data,
            (response)=>{
                login(
                    {
                        username: this.state.email,
                        password: this.state.password
                    },
                    token => {
                        fetchCurrentUser(user => {
                            toast.success("Welcome :)");
                            this.props.history.push(routes.home.path);
                        });
                    },
                    error => {
                        window.location.href = appUrl("/login");
                    }
                );
            },
            (error)=>{
                this.state.formErrors.processResponse(
                    error.response,
                    'Please check your inputs'
                );
                this.setState({ working: false });
            });

    }

    render() {
        const {
            first_name,
            last_name,
            email,
            password,
            password_confirmation,

            working,
            formErrors
        } = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <Card variant="outlined" elevation={0}>
                    <CardHeader title="Create New Account"/>
                    <CardContent>
                        <p className="tac">
                            Don't have an account? Enter your details below to sign up.
                        </p>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <TextField
                                    label="First Name"
                                    value={first_name}
                                    margin="none"
                                    size="small"
                                    placeholder="Joe"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('first_name')}
                                    error={formErrors.has('first_name')}
                                    helperText={formErrors.get('first_name')}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    label="Last Name"
                                    value={last_name}
                                    margin="none"
                                    size="small"
                                    placeholder="Doe"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('last_name')}
                                    error={formErrors.has('last_name')}
                                    helperText={formErrors.get('last_name')}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="Email Address"
                                    value={email}
                                    margin="none"
                                    size="small"
                                    placeholder="name@domain.com"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('email')}
                                    error={formErrors.has('email')}
                                    helperText={formErrors.get('email')}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    type="password"
                                    label="Password"
                                    value={password}
                                    margin="none"
                                    size="small"
                                    placeholder="******"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('password')}
                                    error={formErrors.has('password')}
                                    helperText={formErrors.get('password')}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    type="password"
                                    label="Confirm Password"
                                    value={password_confirmation}
                                    margin="none"
                                    size="small"
                                    placeholder="******"
                                    fullWidth
                                    disabled={working}
                                    onChange={this.handleInputChange('password_confirmation')}
                                    error={formErrors.has('password_confirmation')}
                                    helperText={formErrors.get('password_confirmation')}
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Stack
                            width="100%"
                            direction="row"
                            justifyContent="flex-end"
                            alignItems="center"
                            spacing={2}
                        >
                            <LoadingButton
                                variant="contained"
                                type="submit"
                                color="secondary"
                                loading={working}
                                size="large"
                            >
                                Sign Up
                            </LoadingButton>
                        </Stack>
                    </CardActions>
                </Card>
            </form>
        )
    }
}

export default withRouter(RegisterForm);
