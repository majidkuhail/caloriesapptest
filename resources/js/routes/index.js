import React from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import _values from "lodash/values";
import ScrollToTop from "../common/ScrollToTop";
import { inject, observer } from "mobx-react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import routes from "./routes";
import Page from "../common/Page";
import Header from "../components/Header";
import {ToastContainer} from "react-toastify";

@inject("authStore")
@observer
class Routes extends React.Component {
    render() {
        const {
            authStore
        } = this.props;

        return (
            <Router history={this.props.history}>
                <Route
                    path="*"
                    render={({location}) => {
                        if(location?.location) {
                            location = location.location;
                        }
                        return(
                            <div>
                                <ScrollToTop/>
                                <Header/>
                                <div className="page-wrapper">
                                    <TransitionGroup>
                                        <CSSTransition
                                            key={location.pathname}
                                            classNames="page-fade"
                                            timeout={500}
                                        >
                                            <Switch location={location}>
                                                {_values(routes).map((
                                                        {
                                                            component: Component,
                                                            path: path,
                                                            auth: auth,
                                                            exact: exact,
                                                            guest_only: guest_only,
                                                            title: title
                                                        },
                                                        i
                                                    ) => (
                                                        <Route
                                                            key={i}
                                                            exact={exact}
                                                            path={path}
                                                            render={props => {
                                                                //redirect if authenticated path
                                                                if (
                                                                    auth && !authStore.check
                                                                ) {
                                                                    return (<Redirect to={routes.login.path} />);
                                                                }

                                                                //redirect if guest-onlu path
                                                                if (
                                                                    guest_only && authStore.check
                                                                ) {
                                                                    return (<Redirect to={routes.home.path} />);
                                                                }

                                                                //load dynamic page component
                                                                return (
                                                                    <Page
                                                                        props={props}
                                                                        location={i}
                                                                        component={Component}
                                                                        title={title}
                                                                    />
                                                                );
                                                            }}
                                                        />
                                                    )
                                                )}
                                            </Switch>
                                        </CSSTransition>
                                    </TransitionGroup>
                                </div>
                                <ToastContainer
                                    position="top-center"
                                    newestOnTop
                                    autoClose={3000}
                                    closeOnClick
                                    hideProgressBar={true}
                                    theme="colored"
                                />
                            </div>
                        )}}
                />
            </Router>
        );
    }
}

export default Routes;
