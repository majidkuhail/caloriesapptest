import {routePath} from "../utils";
import Home from "../pages/Home";
import Login from "../pages/Login";

export default {
    //Public
    home: {
        path: routePath(''),
        exact: true,
        auth: true,
        component: Home,
        title: 'Home'
    },
    login: {
        path: routePath('login'),
        exact: true,
        guest_only: true,
        component: Login,
        title: 'Login'
    },
}
