export const OAUTH_URL = process.env.MIX_REACT_APP_OAUTH_URL
export const API_URL = process.env.MIX_REACT_APP_API_URL
export const ADMIN_API_URL = process.env.MIX_REACT_APP_ADMIN_API_URL
export const CLIENT_ID = process.env.MIX_REACT_APP_CLIENT_ID
export const CLIENT_SECRET = process.env.MIX_REACT_APP_CLIENT_SECRET
export const VERSION = process.env.MIX_REACT_APP_VERSION
export const HOME_URL = process.env.MIX_REACT_APP_HOME_URL
export const ADMIN_URL = process.env.MIX_REACT_APP_ADMIN_URL
export const APP_PATH = '/app';
export const TOKEN_COOKIE = 'capp_t';
