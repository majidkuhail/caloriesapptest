import { observable, computed, action } from "mobx";

class AuthStore {
    @observable loggedIn = false;

    @observable token = '';

    @observable user = null;

    constructor() {}

    @computed get check() {
        return this.loggedIn && this.token && (this.token !== '');
    }

    @computed get checkUser() {
        return this.check && this.isUser;
    }

    @computed get checkAdmin() {
        return this.check && this.isAdmin;
    }

    @computed get getUser() {
        return this.user;
    }

    @computed get isUser() {
        return (this.user && this.user.role === 'user')
    }

    @computed get isAdmin() {
        return (this.user && this.user.role === 'admin')
    }

    @action setToken(token) {
        this.token = token;
        this.loggedIn = true;
    }

    @action setUser(user) {
        this.user = user;
    }

    @action forgetToken() {
        this.token = '';
        this.loggedIn = false;
        this.user = null;
    }
}
export default new AuthStore();
