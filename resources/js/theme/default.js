import {createTheme} from "@mui/material";
import {ThemeOptions} from "./options";

const Theme = createTheme(ThemeOptions);

export default Theme;
