export const ThemeOptions = {
    palette: {
        type: 'light',
        primary: {
            main: '#9c27b0',
        },
        secondary: {
            main: '#e7722a',
        },
        error: {
            main: '#d50000',
        },
    },
    typography: {
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            "Roboto",
            "Rubik"
        ],
        fontSize: 14,
        h1: {fontSize: '2rem'},
        h2: {fontSize: '1.6rem'},
        h3: {fontSize: '1.4rem'},
        h4: {fontSize: '1.2rem'},
        h5: {fontSize: '1.2rem', fontWeight: 500},
        h6: {fontSize: '0.8rem'},
    },
    shape: {
        borderRadius: 20
    },
    shadows: {
        0: 'none',
        1: 'none',
        2: 'none',
        3: 'none',
        4: 'none',
        5: 'none',
        6: 'none',
        7: 'none',
        8: 'none',
        9: 'none',
        10: 'none',
        11: 'none',
        12: 'none',
        13: 'none',
        14: 'none',
        15: 'none',
        16: 'none',
        17: 'none',
        18: 'none',
        19: 'none',
        20: 'none',
        22: 'none',
        23: 'none',
        24: 'none'
    },
    components: {
        MuiPaper: {
            styleOverrides:{
                elevation8:{
                    boxShadow: "0px 3px 6px 4px rgb(0,0,0,0.08)",
                }
            }
        },
        MuiCard: {
            styleOverrides: {
                root: {
                    marginBottom: 10,
                    // boxShadow: 'none'
                }
            }
        },
        MuiCardContent: {
            styleOverrides: {
                root: {
                    '&:last-child': {
                        paddingBottom: 16
                    }
                }
            }
        },
        MuiButton: {
            styleOverrides: {
                root: {
                    boxShadow: 'none',
                    textTransform: 'none'
                }
            }
        },
        MuiButtonBase: {
            styleOverrides: {
                root: {
                    boxShadow: 'none',
                    textTransform: 'none'
                }
            }
        },
        MuiLoadingButton: {
            styleOverrides: {
                root: {
                    boxShadow: 'none',
                    textTransform: 'none'
                }
            }
        },
        MuiCardHeader: {
            styleOverrides: {
                root: {
                    borderBottom: '1px solid #f5f5f5',
                    padding: '8px 16px',
                }
            }
        },
        MuiCardActions: {
            styleOverrides: {
                root: {
                    borderTop: '1px solid #f5f5f5'
                }
            }
        },
        MuiInputBase: {
            styleOverrides: {
                root: {
                    fontSize: '1rem'
                }
            }
        },
        MuiInputLabel: {
            styleOverrides: {
                root: {
                    fontSize: '1rem'
                }
            }
        },
        MuiAccordion: {
            styleOverrides: {
                root: {
                    borderRadius: 20,
                    marginBottom: 16,
                    '&:before': {
                        opacity: 0
                    }
                }
            }
        },
        MuiDialog:{
            backdrop:{
                // background: 'rgba(0, 0, 0, 0.04)'
            }
        }
    }
};
