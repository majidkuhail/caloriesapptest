import {API_URL, HOME_URL, APP_PATH} from "../settings";

export function getElementAttributes(el) {
    const a = {};
    const atts = el.attributes;
    for (let i = 0; i < atts.length; i++) {
        a[atts[i].nodeName] = atts[i].nodeValue;
    }
    return a;
}

export function apiUrl(path) {
    return API_URL + (path.charAt(0) === "/" ? "" : "/") + path;
}

export function getUrl(path) {
    if(path.startsWith('https://') || path.startsWith('http://')){
        return path;
    }
    return HOME_URL + (path.charAt(0) === "/" ? "" : "/") + path;
}
export function appUrl(path) {
    return (
        `${HOME_URL+APP_PATH}${path.charAt(0) === "/" ? "" : "/"  }${path}`
    );
}
export function routePath(route) {
    const path = typeof route === "string" ? route : route.path;
    return `${ APP_PATH }${path.charAt(0) === "/" ? "" : "/"  }${path}`;
}

export function routeAssignParams(route, params) {
    let {path} = route;
    if (path && params) {
        for (const key in params) {
            if (path.indexOf(`:${  key}`)) {
                path = path.replace(`:${  key}`, params[key]);
                path = path.replace("?", "");
            }
        }
    }
    return path;
}

export function updatePageTitle(title){
    try {
        if (window.app_origTitle === undefined) {
            window.app_origTitle = document.title;
        }
        document.title = title ? `${title} - Calories App` : window.app_origTitle;
    }catch (e){
        console.log('error:', e);
    }
}
