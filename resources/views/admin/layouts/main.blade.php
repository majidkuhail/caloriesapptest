@include('admin.partials.head')

@yield('content')

@include('admin.partials.footer')
