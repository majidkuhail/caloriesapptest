<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->middleware(['auth:api','auth.admin'])->group(function() {
    Route::get('dashboard/stats', 'DashboardController@stats');

    Route::resource('users', 'UserController')->except([
        'create', 'edit'
    ]);

    Route::get('user_food_entries/{user}', 'FoodEntryController@index');
    Route::get('user_food_entries/{user}/stats', 'FoodEntryController@stats');
    Route::post('user_food_entries/{user}', 'FoodEntryController@store');
    Route::resource('food_entries', 'FoodEntryController')->only([
        'show', 'update', 'destroy'
    ]);
});
