<?php

use Illuminate\Support\Facades\Route;


Route::get('{path?}', [
    'uses' => 'AdminAppController@index',
    'as' => 'admin.app',
    'where' => ['path' => '.*']
]);
