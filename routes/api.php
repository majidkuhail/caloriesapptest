<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
    //public routes
    Route::post('user/register', 'UserController@register');

    //common auth routes
    Route::middleware(['auth:api'])->group(function () {
        Route::get('user', 'UserController@me');
    });

    //regular user onlu auth routes
    Route::middleware(['auth:api','auth.user'])->group(function () {
        Route::put('user/calories_limit', 'UserController@updateCaloriesLimit');

        Route::resource('food_entries', 'FoodEntryController')->except([
            'create', 'edit'
        ]);
    });
});
